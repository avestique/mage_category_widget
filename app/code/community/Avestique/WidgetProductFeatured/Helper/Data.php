<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Data.php
 * @class Avestique_CatalogMenuWidget_Helper_Data
 */

class Avestique_WidgetProductFeatured_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_CONFIG_WIDGET_CONFIG_CATEGORY_ADMIN_VIEW_TYPE = 'av_widget_product_featured/product_featured/admin_category_view_type';

    const XML_PATH_WIDGET_OPTION_TITLE                = 'title';
    const XML_PATH_WIDGET_OPTION_VIEW_TYPE            = 'view_type';
    const XML_PATH_WIDGET_OPTION_SELECT_PRODUCT_TYPE  = 'select_product_type';
    const XML_PATH_WIDGET_OPTION_SELECTED_CATEGORY    = 'selected_category';
    const XML_PATH_WIDGET_OPTION_SELECTED_PRODUCT     = 'selected_product';
    const XML_PATH_WIDGET_OPTION_CATEGORY_SORT_BY     = 'category_sort_by';
    const XML_PATH_WIDGET_OPTION_CATEGORY_SORT_TYPE   = 'category_sort_type';
    const XML_PATH_WIDGET_OPTION_CATEGORY_LIMIT       = 'category_limit';
    const XML_PATH_WIDGET_OPTION_CATEGORY_INFO        = 'category_info';
    const XML_PATH_WIDGET_OPTION_PRODUCT_SORT_BY      = 'product_sort_by';
    const XML_PATH_WIDGET_OPTION_PRODUCT_SORT_TYPE    = 'product_sort_type';
    const XML_PATH_WIDGET_OPTION_PRODUCTS_LIMIT       = 'products_limit';
    const XML_PATH_WIDGET_OPTION_PRODUCT_VIEW_TYPE    = 'product_view_type';
    const XML_PATH_WIDGET_OPTION_PRODUCT_GRID_COLUMNS = 'product_grid_columns';

    public function getTitle(Mage_Widget_Block_Interface $widget)
    {
        if ($widget->getData(self::XML_PATH_WIDGET_OPTION_TITLE))
        {
            return $widget->getData(self::XML_PATH_WIDGET_OPTION_TITLE);
        }

        return '';
    }

    public function getViewType(Mage_Widget_Block_Interface $widget)
    {
        if ($widget->getData(self::XML_PATH_WIDGET_OPTION_SELECT_PRODUCT_TYPE) == 1)
        {
            return 0;
        }
        else
        {
            return (int) $widget->getData(self::XML_PATH_WIDGET_OPTION_VIEW_TYPE);
        }
    }

    public function getProductSelectionType(Mage_Widget_Block_Interface $widget)
    {
        return (int) $widget->getData(self::XML_PATH_WIDGET_OPTION_SELECT_PRODUCT_TYPE);
    }

    public function getProductViewType(Mage_Widget_Block_Interface $widget)
    {
        if (in_array($this->getViewType($widget), array(0,2,3)))
        {
            if ($widget->getData(self::XML_PATH_WIDGET_OPTION_PRODUCT_VIEW_TYPE)==1)
                return 'grid';
            else
                return 'list';
        }

        return NULL;
    }

    public function getCategorySortBy(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_CATEGORY_SORT_BY);
    }

    public function getCategorySortType(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_PRODUCT_SORT_TYPE);
    }

    public function getCategoryLimit(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_CATEGORY_LIMIT);
    }

    public function getSelectedCategories(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_SELECTED_CATEGORY);
    }

    public function getProductSortBy(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_PRODUCT_SORT_BY);
    }

    public function getProductSortType(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_CATEGORY_SORT_TYPE);
    }

    public function getProductLimit(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_PRODUCTS_LIMIT);
    }

    public function getSelectedProducts(Mage_Widget_Block_Interface $widget)
    {
        return $widget->getData(self::XML_PATH_WIDGET_OPTION_SELECTED_PRODUCT);
    }

    public function getUniqueKey()
    {
        return hash('whirlpool', microtime() . rand(1000, 9999));
    }

    public function getAdminCategoryViewType()
    {
        return Mage::getStoreConfig(self::XML_PATH_CONFIG_WIDGET_CONFIG_CATEGORY_ADMIN_VIEW_TYPE);
    }
}