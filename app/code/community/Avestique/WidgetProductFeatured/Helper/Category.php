<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Category.php
 */
class Avestique_WidgetProductFeatured_Helper_Category extends Mage_Core_Helper_Abstract
{

    /* get no active parent nodes */
    public function getCountNoActiveParent(Mage_Catalog_Model_Category $selectedCategoryObject)
    {
        $collection = Mage::getModel('catalog/category')->getCollection()
            ->addFieldToFilter( Mage::getModel('catalog/category')->getIdFieldName(), array( 'in' => $selectedCategoryObject->getParentIds()) )
            ->addFieldToFilter( 'is_active', array( 'eq' => 0 ))
            ->setStoreId(Mage::app()->getStore()->getId())
        ->setOrder('position', Varien_Db_Select::SQL_ASC);

        return count($collection);
    }

    public function getRandValues($objectList, $limit)
    {
        if (is_array($objectList) && count($objectList) > 0 && $limit > 0)
        {
            $_categories = array_rand($objectList, $limit);

            foreach($objectList as $index => $object)
                if (!in_array($index, $_categories))
                    unset($objectList[$index]);
        }

        return $objectList;
    }

    public function getSortValues($objectList, $limit)
    {
        $iterator = 0;

        if ($limit > 0)
            foreach($objectList as $index => $object)
                if ( ++$iterator > $limit )
                    unset($objectList[$index]);

        return $objectList;
    }

    public function getSubcategoryDepth($depth)
    {
        $depth = (int) $depth;

        if ((int) Mage::app()->getStore()->getConfig('av_widget_product_featured/options/max_depth') > 0)
            $depth = $depth > (int) Mage::app()->getStore()->getConfig('av_widget_product_featured/options/max_depth') ?
                (int) Mage::app()->getStore()->getConfig('av_widget_product_featured/options/max_depth') :
                $depth;

        return $depth;
    }

    public function checkCategoryIDs($ids)
    {
        $ids = explode(',', $ids);

        foreach($ids as $id => $item)
        {
            if (! (int) $item)
                unset($ids[$id]);
        }

        return $ids;
    }

    public function getRandCategory($ids)
    {
        if (is_array($ids))
        {
            while(count($ids) > 0)
            {
                $index = array_rand($ids);
                $selectedCategoryObject = Mage::getModel('catalog/category')->load($ids[$index]);
                //$selectedCategory = $ids[$index];

                unset($ids[$index]);

                if ($selectedCategoryObject->getIsActive())
                    break;
                else
                    $selectedCategoryObject = NULL;
            }

            if (isset($selectedCategoryObject) && $selectedCategoryObject && $selectedCategoryObject->getIsActive()) return $selectedCategoryObject;
        }

        return NULL;
    }

    public function sortProductCategories(Mage_Catalog_Model_Resource_Collection_Abstract $subCats)
    {
        $categoriesForProducts = array();

        foreach($subCats as $_subCategory)
            if($_subCategory->getIsActive() && !$this->getCountNoActiveParent($_subCategory))
                $categoriesForProducts[$_subCategory->getId()] = $_subCategory;

        return $categoriesForProducts;
    }

    public function getProductCollection($_currentCategory)
    {
        $layer = Mage::getSingleton('catalog/layer');

        $layer->setCurrentCategory($_currentCategory);

        $productCollection = Mage::getResourceModel('catalog/product_collection')
                                ->addFieldToFilter( 'visibility', array( 'in' => array(2,4) ))
                                ->addAttributeToSelect('*');

        $layer->prepareProductCollection($productCollection);


        $_products = $layer->getProductCollection();

        return  $_products;
    }
}