<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Products.php
 */

class Avestique_WidgetProductFeatured_Block_Catalog_Product extends Mage_Core_Block_Template
{
    const WIDGET_TEMPLATE_PATH = 'avestique/widget/product_featured/list/products.phtml';

    public function _construct()
    {
        $this->setTemplate(self::WIDGET_TEMPLATE_PATH);
        parent::_construct();
    }

    public function _beforeToHtml()
    {
        $widget = $this->getWidget();
        $randCategories = $this->getRandCategories();

        if ($widget)
        {
            $typeSelection = Mage::helper('av_widget_product_featured')->getProductSelectionType($widget);

            $productCollection = Mage::getModel('av_widget_product_featured/product', array(
                'products' => $typeSelection ? Mage::helper('av_widget_product_featured')->getSelectedProducts($widget) : NULL,
                'categories' => !$typeSelection ? ($randCategories ? $randCategories : Mage::helper('av_widget_product_featured')->getSelectedCategories($widget)) : NULL,
                'sort_by' => Mage::helper('av_widget_product_featured')->getProductSortBy($widget),
                'sort_type' => Mage::helper('av_widget_product_featured')->getProductSortType($widget),
                'sort_limit' => Mage::helper('av_widget_product_featured')->getProductLimit($widget)
            ))->getCollection();

            $this->setProductCollection($productCollection);
        }

        parent::_beforeToHtml();
    }
}