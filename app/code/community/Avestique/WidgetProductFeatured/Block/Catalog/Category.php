<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Categories.php
 */

class Avestique_WidgetProductFeatured_Block_Catalog_Category extends Mage_Core_Block_Template
{
    const WIDGET_TEMPLATE_PATH = 'avestique/widget/product_featured/list/categories.phtml';

    protected $_randCategories = array();

    public function _construct()
    {
        $this->setTemplate(self::WIDGET_TEMPLATE_PATH);
        parent::_construct();

        $widget = $this->getWidget();

        if ($widget)
        {
            $categoryCollection = Mage::getModel('av_widget_product_featured/category', array(
                'categories' => Mage::helper('av_widget_product_featured')->getSelectedCategories($widget),
                'type' => Mage::helper('av_widget_product_featured')->getViewType($widget),
                'sort_by' => Mage::helper('av_widget_product_featured')->getCategorySortBy($widget),
                'sort_type' => Mage::helper('av_widget_product_featured')->getCategorySortType($widget),
                'sort_limit' => Mage::helper('av_widget_product_featured')->getCategoryLimit($widget)
            ))->getCollection();

            $this->setCategoryCollection($categoryCollection);

            foreach($categoryCollection as $item)
            {
                $this->_randCategories[] = $item->getId();
            }
        }
    }

    public function _beforeToHtml()
    {
        parent::_beforeToHtml();
    }

    public function getCategories()
    {
        if (count($this->_randCategories) > 0)
        {
            return $this->_randCategories;
        }
        else
        {
            return explode(',', Mage::helper('av_widget_product_featured')->getSelectedCategories($this->getWidget()));
        }
    }
}