<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Chooser.php
 */

class Avestique_WidgetProductFeatured_Block_Adminhtml_Chooser_Product_Category_Chooser extends Mage_Adminhtml_Block_Catalog_Category_Widget_Chooser
{
    /**
     * Tree JSON source URL
     *
     * @return string
     */
    public function getLoadTreeUrl($expanded=null)
    {
        return $this->getUrl('av_widget_product_featured/adminhtml_catalog_product_multiproducts/categoriesJson', array(
            '_current'=>true,
            'uniq_id' => $this->getId(),
            'use_massaction' => $this->getUseMassaction()
        ));
    }
}