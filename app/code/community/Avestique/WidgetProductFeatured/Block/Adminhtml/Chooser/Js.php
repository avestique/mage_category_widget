<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Varien
 * @package    Varien_Data
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Form text element
 *
 * @category   Varien
 * @package    Varien_Data
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Avestique_WidgetProductFeatured_Block_Adminhtml_Chooser_Js extends Varien_Data_Form_Element_Abstract
{
    public function getHtml()
    {
        return "<script type=\"text/javascript\">
                FormElementDependenceController.prototype.trackChange = function(e, idTo, valuesFrom)
                {
                    // define whether the target should show up
                    var shouldShowUp = true;
                    for (var idFrom in valuesFrom) {
                        var from = $(idFrom);
                        if (valuesFrom[idFrom] instanceof Array) {
                            if (!from || valuesFrom[idFrom].indexOf(from.value) == -1) {
                                shouldShowUp = false;
                            }
                        } else {
                            if (!from || from.value != valuesFrom[idFrom]) {
                                shouldShowUp = false;
                            }
                        }
                    }

                    if (shouldShowUp) {
                        var currentConfig = this._config;
                        $(idTo).up(this._config.levels_up).select('input', 'select', 'td').each(function (item) {
                            if ((!item.type || item.type != 'hidden') && !($(item.id+'_inherit') && $(item.id+'_inherit').checked)
                                && !(currentConfig.can_edit_price != undefined && !currentConfig.can_edit_price)) {
                                item.disabled = false;
                            }
                        });

                        $(idTo).up(this._config.levels_up).show();

                        levels_up = this._config.levels_up;

                        $$('.' + idTo).each(function (item){

                            if ($(item).hasClassName('required-entry-marker'))
                                $(item).addClassName('required-entry');

                            $(item).up(levels_up).show();
                        });
                    } else {
                        $(idTo).up(this._config.levels_up).select('input', 'select', 'td').each(function (item){
                            // don't touch hidden inputs (and Use Default inputs too), bc they may have custom logic
                            if ((!item.type || item.type != 'hidden') && !($(item.id+'_inherit') && $(item.id+'_inherit').checked)) {
                                item.disabled = true;
                            }
                        });

                        $(idTo).up(this._config.levels_up).hide();

                        levels_up = this._config.levels_up;

                        $$('.' + idTo).each(function (item){

                            if ($(item).hasClassName('required-entry-marker'))
                                $(item).removeClassName('required-entry');

                            $(item).up(levels_up).hide();
                        });
                    }
                }
        </script>";
    }
}
