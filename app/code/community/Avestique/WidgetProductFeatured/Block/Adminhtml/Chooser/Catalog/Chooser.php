<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Select.php
 */
class Avestique_WidgetProductFeatured_Block_Adminhtml_Chooser_Catalog_Chooser extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories//Mage_Adminhtml_Block_Catalog_Category_Widget_Chooser
{
    /**
     * Specify template to use
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('avestique/widget/product_featured/categories.phtml');
    }


    /**
     * Return array with category IDs which the product is assigned to
     *
     * @return array
     */
    protected function getCategoryIds()
    {
        $categories = $this->getRequest()->getParam('element_value');
        return explode(',', $categories);
    }

    /**
     * Checks when this block is readonly
     *
     * @return boolean
     */
    public function isReadonly()
    {
        return false;
    }

    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $selected = explode(',', $element->getValue());

        $uniqId = Mage::helper('core')->uniqHash($element->getId());
        $sourceUrl = $this->getUrl('av_widget_product_featured/adminhtml_catalog_category_widget/chooser', array('uniq_id' => $uniqId, 'use_massaction' => true, 'ids' => $element->getValue()));


        $chooser = $this->getLayout()->createBlock('av_widget_product_featured/adminhtml_chooser_catalog_row_field')
            ->setElement($element)
            ->setTranslationHelper($this->getTranslationHelper())
            ->setConfig($this->getConfig())
            ->setFieldsetId($this->getFieldsetId())
            ->setSourceUrl($sourceUrl)
            ->setUniqId($uniqId);

        if ($element->getValue()) {
            if (count($selected) > 0)
            {
                $categoryCollection = Mage::getSingleton('catalog/category')->getCollection()
                    ->addAttributeToSelect('name')
                    ->addIdFilter($selected);

                $labels = array();

                foreach($categoryCollection as $category)
                {
                    $labels[] = $category->getName();
                }

                $chooser->setLabel(implode(',', $labels));
            }

            /*$categoryId = false;
            if (isset($value[0]) && isset($value[1]) && $value[0] == 'category') {
                $categoryId = $value[1];
            }
            if ($categoryId) {
                $label = Mage::getSingleton('catalog/category')->load($categoryId)->getName();
                $chooser->setLabel($label);
            }*/
        }

        $element->setData('after_element_html', $chooser->toHtml());
        return $element;
    }


    /**
     * Prepare html output
     *
     * @return string
     */
    protected function getButtonOK()
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
            'label'     => Mage::helper('av_widget_product_featured')->__('Save'),
            'onclick'   => "return okButton.onPress()",
            'class'     => "",
            'type'      => 'button',
            'style'     => 'margin:20px 0 10px;',
            'id'        => "",
        ))->toHtml();
    }

    public function useLayeredCategory()
    {
        return Mage::helper('av_widget_product_featured')->getAdminCategoryViewType();
    }
}