<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Catalog.php
 * @class Avestique_Widget_Block_Catalog
 */

class Avestique_WidgetProductFeatured_Block_Catalog
    extends Mage_Core_Block_Template
        implements Mage_Widget_Block_Interface
{

    /**
     * widget.xml default parameters
     *
     * @var array
     */
    protected $_parameterList = array(
        'selected_category'     => array(),
        'render_type'           => 0,
        'categories_to_show'    => 0,
        'depth_to_select'       => 0,
        'rand_category_info'    => 0,
        'subcategories_option'  => 0,

        'products_option'       => 0,
        'products_to_show'      => 0,
        'product_parent_depth'  => 0
    );

    protected $_subCategories = array();

    protected $_products = array();

    protected function _setParameters()
    {
        if ($this->getData('selected_category'))
            $this->_parameterList['selected_category'] = Mage::helper('av_widget_product_featured/category')->checkCategoryIDs($this->getData('selected_category'));

        if ($this->getData('render_type'))
            $this->_parameterList['render_type'] = 1;

        if ($this->getData('categories_to_show'))
            $this->_parameterList['categories_to_show'] = (int) $this->getData('categories_to_show');

        if ($this->getData('depth_to_select'))
            $this->_parameterList['depth_to_select'] = Mage::helper('av_widget_product_featured/category')->getSubcategoryDepth($this->getData('depth_to_select'));

        if ($this->getData('rand_category_info'))
            $this->_parameterList['rand_category_info'] = 1;

        if ($this->getData('subcategories'))
            $this->_parameterList['subcategories_option'] = (int) $this->getData('subcategories');

        if ($this->getData('products'))
            $this->_parameterList['products_option'] = (int) $this->getData('products');

        if ($this->getData('products_to_show'))
            $this->_parameterList['products_to_show'] = (int) $this->getData('products_to_show');

        if ($this->getData('product_parent_depth'))
            $this->_parameterList['product_parent_depth'] = (int) $this->getData('product_parent_depth');
    }

    public function getWidgetParameter($code)
    {
        if (isset($this->_parameterList[$code]))
        {
            return $this->_parameterList[$code];
        }

        return FALSE;
    }

    public function _construct()
    {
        parent::_construct();

        $this->_setParameters();

        $this->setTemplate('catalog/navigation/widget/category_list.phtml');
    }

    public function sortCategories(Mage_Catalog_Model_Resource_Collection_Abstract $subCats)
    {
        foreach($subCats as $_subCategory)
            if($_subCategory->getIsActive() && !Mage::helper('av_widget_product_featured/category')->getCountNoActiveParent($_subCategory))
                $this->_subCategories[$_subCategory->getId()] = $_subCategory;
    }

    public function prepareCategory($selectedCategory)
    {
        $selectedCategoryObject = Mage::getModel('catalog/category')->load($selectedCategory);

        /* if all parent and current element is active */
        if ($selectedCategoryObject->getIsActive() && !Mage::helper('av_widget_product_featured/category')->getCountNoActiveParent($selectedCategoryObject))
        {
            $this->_subCategories[$selectedCategoryObject->getId()] = $selectedCategoryObject;

            if ($this->getWidgetParameter('subcategories_option')!=2 || ( in_array($this->getWidgetParameter('products_option'), array(0,1)) && $this->getWidgetParameter('product_parent_depth')) )
            {
                $subCats = Mage::getModel('catalog/category')->getCategories($selectedCategory, $this->getWidgetParameter('depth_to_select'), true, true);

                $this->sortCategories($subCats);
            }
        }
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        echo '<pre><br/>';
        var_dump("!");
        die();
        if ($ids = $this->getWidgetParameter('selected_category'))
        {
            /* categories */
            if ($this->getWidgetParameter('render_type'))
            {
                if ($selectedCategoryObject = Mage::helper('av_widget_product_featured/category')->getRandCategory($ids))
                {
                    $this->prepareCategory($selectedCategoryObject->getId());

                    if ($this->getWidgetParameter('rand_category_info'))
                        $this->assign('root_category_info_id', $selectedCategoryObject->getId());

                }
            }
            else
            {
                foreach($ids as $selectedCategory)
                {
                    $this->prepareCategory($selectedCategory);
                }
            }

            /* products */
            if (in_array($this->getWidgetParameter('products_option'), array(0,1)))
            {
                $catsID = array();

                if ($this->getWidgetParameter('product_parent_depth'))
                {
                    $catsID = array_keys($this->_subCategories);
                }
                else
                {
                    if (isset($selectedCategoryObject) && $selectedCategoryObject->getId())
                        $catsID = array($selectedCategoryObject->getId());
                    else
                        $catsID = $ids;
                }

                foreach($catsID as $id)
                {
                    if (isset($this->_subCategories[$id]))
                    {
                        $_currentCategory = $this->_subCategories[$id];
                    }
                    else
                    {
                        $_currentCategory = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($id);//->addFieldToFilter( 'is_active', array( 'eq' => 0 ) )
                    }



                    if ($_currentCategory && $_currentCategory->getId() && $_currentCategory->getIsActive())
                    {
                        $_products = Mage::helper('av_widget_product_featured/category')->getProductCollection($_currentCategory);

                        foreach($_products as $_product)
                        {
                            $this->_products[$_product->getId()] = $_product;
                        }
                    }
                }

                if ($this->getWidgetParameter('product_parent_depth') && $this->getWidgetParameter('subcategories_option')==2)
                {
                    foreach($this->_subCategories as $key => $item)
                        if (!in_array($key, $ids))
                            unset($this->_subCategories[$key]);

                    //$this->_subCategories = array_intersect($catsID, $ids);
                }
            }
        }

        switch($this->getWidgetParameter('products_option'))
        {
            case 0:
                $this->assign('products', Mage::helper('av_widget_product_featured/category')->getRandValues($this->_products, $this->getWidgetParameter('products_to_show')) );
                break;
            case 1:
                $this->assign('products', Mage::helper('av_widget_product_featured/category')->getSortValues($this->_products, $this->getWidgetParameter('products_to_show')) );
                break;
            default:
                $this->assign('products', NULL);
                break;
        }

        switch($this->getWidgetParameter('subcategories_option'))
        {
            case 0:
                $this->assign('categories', Mage::helper('av_widget_product_featured/category')->getRandValues($this->_subCategories, $this->getWidgetParameter('categories_to_show')) );
                break;
            case 1:
                $this->assign('categories', Mage::helper('av_widget_product_featured/category')->getSortValues($this->_subCategories, $this->getWidgetParameter('categories_to_show')) );
                break;
            default:
                $this->assign('categories', NULL);
                break;
        }

        $this->assign('show_sub_categories', $this->getWidgetParameter('subcategories_option') != 2 );

        return parent::_toHtml();
    }
}