<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Catalog.php
 * @class Avestique_Widget_Block_Catalog
 */

class Avestique_WidgetProductFeatured_Block_Catalog extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    const WIDGET_TEMPLATE_PATH = 'avestique/widget/product_featured/list.phtml';

    public function _construct()
    {
        $this->setUniqueKey(MD5(microtime()));
        $this->setTemplate(self::WIDGET_TEMPLATE_PATH);
        parent::_construct();
    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $shortCacheId = array(
            'CATALOG_NAVIGATION',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            'name' => $this->getNameInLayout(),
            $this->getCurrenCategoryKey()
        );

        $shortCacheId = implode('|', $shortCacheId);
        $shortCacheId = md5($shortCacheId);

        $cacheId['category_path'] = $this->getCurrenCategoryKey();
        $cacheId['short_cache_id'] = $shortCacheId;

        return $cacheId;
    }

    protected function _prepareLayout()
    {
        $this->setTitle(Mage::helper('av_widget_product_featured')->getTitle($this));
        $this->setViewType(Mage::helper('av_widget_product_featured')->getViewType($this));

        if (in_array($this->getViewType(), array(1,2,3)))
        {
            $blockCat = $this->getLayout()->createBlock(
                'av_widget_product_featured/catalog_category',
                "av.featured.categories.{$this->getUniqueKey()}",
                array(
                    'widget' => $this,
                    'type' => $this->getViewType()
                )
            );

            $this->setChild("av.featured.categories.{$this->getUniqueKey()}", $blockCat);
        }

        if (in_array($this->getViewType(), array(0,2,3)))
        {
            $randCategories = isset($blockCat) && count($blockCat->getCategories()) > 0 ? $blockCat->getCategories() : NULL;
            $block = $this->getLayout()->createBlock(
                'av_widget_product_featured/catalog_product',
                "av.featured.products.{$this->getUniqueKey()}",
                array(
                    'widget' => $this,
                    'rand_categories' => $randCategories
                )
            );

            $this->setChild("av.featured.products.{$this->getUniqueKey()}", $block);
        }

        parent::_prepareLayout();
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {


        /*if (view_type == 1) //show only products
            product_sort_by
            product_sort_type
            products_limit
            product_view_type

            if (product_view_type==1)
                product_grid_columns
        elseif (view_type == 2) //show only selected categories
            category_sort_by
            category_sort_type
            category_limit
            if (category_limit==1)
                category_info
        elseif (view_type == 3) //show top categories and products
            category_sort_by
            category_sort_type
            category_limit
            if (category_limit==1)
                category_info

            product_sort_by
            product_sort_type
            products_limit
            product_view_type

            if (product_view_type==1)
                product_grid_columns
        elseif (view_type == 4) // show selected categories and products
            category_sort_by
            category_sort_type
            category_limit
            if (category_limit==1)
                category_info

            product_sort_by
            product_sort_type
            products_limit
            product_view_type

            if (product_view_type==1)
                product_grid_columns*/

        return parent::_toHtml();
    }
}