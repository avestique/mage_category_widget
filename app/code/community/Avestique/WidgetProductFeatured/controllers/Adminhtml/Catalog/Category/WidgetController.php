<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file WidgetController.php
 */

require_once 'Mage/Adminhtml/controllers/Catalog/Category/WidgetController.php';

/**
 * Catalog category widgets controller for CMS WYSIWYG
 */
class Avestique_WidgetProductFeatured_Adminhtml_Catalog_Category_WidgetController extends Mage_Adminhtml_Catalog_Category_WidgetController
{
    protected function _getCategoryTreeBlock()
    {
        //$categories = $this->getRequest()->getPost('element_value', NULL);
        $categories = $this->getRequest()->getPost('element_value', NULL);

        $chooser = $this->getLayout()->createBlock('av_widget_product_featured/adminhtml_chooser_catalog_chooser', '', array(
            'id' => $this->getRequest()->getParam('uniq_id'),
            'use_massaction' => $this->getRequest()->getParam('use_massaction', false)
        ));

        if ($categories)
            $chooser->setSelectedCategories(explode(',', $categories));

        return $chooser;
    }

    /**
     * Categories tree node (Ajax version)
     */
    public function categoriesJsonAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('av_widget_product_featured/adminhtml_chooser_catalog_category_tree')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }
}
