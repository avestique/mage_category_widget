<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Observer.php
 */

class Avestique_WidgetProductFeatured_Model_Adminhtml_Observer
{
    public function rewriteJSFormElementDependenceController($event)
    {
        if ($event->getEvent()->getBlock() instanceof Mage_Widget_Block_Adminhtml_Widget_Options)
        {
            $block = $event->getEvent()->getBlock();

            if ($block->getData('widget_type') == 'av_widget_product_featured/catalog')
            {
                $main_fieldset = $block->getData('main_fieldset');

                $main_fieldset->addType('javascript', 'Avestique_WidgetProductFeatured_Block_Adminhtml_Chooser_Js');
                $main_fieldset->addField('custom_js', 'javascript', $data = array(
                    'name'      => 'js',
                    'label'     => 'js'
                ));
            }
        }
    }
}