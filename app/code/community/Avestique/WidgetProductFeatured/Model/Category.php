<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Category.php
 */

class Avestique_WidgetProductFeatured_Model_Category extends Mage_Core_Model_Abstract
{
    protected $_collection = NULL;

    public function getCollection()
    {
        if (!$this->_collection)
        {
            $limit = NULL;

            if ($this->getSortBy()==0)
                $sortField = 'name';

            if ($this->getSortBy()==1)
                $sortField = 'position';

            if ($this->getSortLimit())
                $limit =  $this->getSortLimit();


            if ($flatFlag = Mage::getStoreConfigFlag(Mage_Catalog_Helper_Category_Flat::XML_PATH_IS_ENABLED_FLAT_CATALOG_CATEGORY))
            {
                $this->_collection = Mage::getResourceModel('catalog/category_flat_collection')
                    ->addIdFilter($this->getCategories())
                    ->addNameToResult()
                    ->addAttributeToSelect('image')
                    ->addAttributeToSelect('description')
                    ->addUrlRewriteToResult()
                    ->addStoreFilter()
                    ->addIsActiveFilter();

                if ($limit)
                    $this->_collection->getSelect()->limit($limit);

                if ($this->getSortBy())
                {
                    if ($sortField && $this->getSortType()!=2)
                    {
                        $this->_collection->addAttributeToSort($sortField, $this->getSortType() ? Varien_Data_Collection::SORT_ORDER_DESC : Varien_Data_Collection::SORT_ORDER_ASC );
                    }
                    else
                    {
                        $this->_collection->getSelect()->order('rand()');
                    }
                }
                else
                {
                    $this->_collection->getSelect()->order('rand()');
                }
            }
            else
            {
                $this->_collection = Mage::getResourceModel('catalog/category_collection')
                    ->addIdFilter($this->getCategories())
                    ->addNameToResult()
                    ->addAttributeToSelect('image')
                    ->addAttributeToSelect('description')
                    ->addUrlRewriteToResult()
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->addIsActiveFilter();


                if ($limit)
                    $this->_collection->getSelect()->limit($limit);

                if ($this->getSortBy())
                {
                    if ($sortField && $this->getSortType()!=2)
                    {
                        $this->_collection->setOrder($sortField, $this->getSortType() ? Varien_Data_Collection::SORT_ORDER_DESC : Varien_Data_Collection::SORT_ORDER_ASC );
                    }
                    else
                    {
                        $this->_collection->getSelect()->order('rand()');
                    }
                }
                else
                {
                    $this->_collection->getSelect()->order('rand()');
                }
                //print $this->_collection->getSelect(); die();
            }
        }

        return $this->_collection;
    }
}