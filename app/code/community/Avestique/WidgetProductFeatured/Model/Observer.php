<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Observer.php
 */

class Avestique_WidgetProductFeatured_Model_Observer
{
    public function checker()
    {
        $productCollection = Mage::getModel('av_widget_product_featured/product', array(
            'products' => NULL,
            'categories' => '13,12,18',
            'sort_by' => null, //Mage::helper('av_widget_product_featured')->getProductSortBy($widget),
            'sort_type' => null, //Mage::helper('av_widget_product_featured')->getProductSortType($widget),
            'sort_limit' => 10//Mage::helper('av_widget_product_featured')->getProductLimit($widget)
        ))->getCollection();
    }
}