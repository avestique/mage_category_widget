<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_WidgetProductFeatured
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Product.php
 */

class Avestique_WidgetProductFeatured_Model_Product extends Mage_Core_Model_Abstract
{
    protected $_collection = NULL;

    public function getCollection()
    {
        if (!$this->_collection)
        {
            $limit = NULL;

            if ($this->getSortBy()==0)
                $sortField = 'name';

            if ($this->getSortBy()==1)
                $sortField = 'position';

            if ($this->getSortLimit())
                $limit = $this->getSortLimit();


            if (0)//$flatFlag = Mage::getStoreConfigFlag(Mage_Catalog_Helper_Category_Flat::XML_PATH_IS_ENABLED_FLAT_CATALOG_CATEGORY))
            {
                $this->_collection = Mage::getResourceModel('catalog/category_flat_collection')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                    ->addNameToResult()
                    ->addUrlRewriteToResult()
                    ->addStoreFilter()
                    ->addIsActiveFilter();

                //$this->_collection->addAttributeToFilter('status', array('in'=>Mage::getSingleton('catalog/product_status')->getVisibleStatusIds()));
                //$this->_collection->addAttributeToFilter('visibility', array('in'=>Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds()));

                if ($this->getCategories())
                {
                    $this->_collection->getSelect()->joinLeft(
                        array('p' => $this->_collection->getTable('catalog/category_product')),
                             'p.product_id = entity_id',
                        array('category_id')
                    );

                    $this->_collection
                        ->addAttributeToFilter( 'p.category_id', array('in' => is_array($this->getCategories()) ? $this->getCategories() : explode(',', $this->getCategories())) );

                }
                else if ($this->getProducts())
                {
                    $this->_collection->addIdFilter($this->getProducts());
                }


                //$this->_collection->groupByAttribute('entity_id');//->removeFieldFromSelect('p.category_id');

                if ($limit)
                    $this->_collection->getSelect()->limit($limit);

                if ($this->getSortBy())
                {
                    if ($sortField && $this->getSortType()!=2)
                    {
                        $this->_collection->addAttributeToSort($sortField, $this->getSortType() ? Varien_Data_Collection::SORT_ORDER_DESC : Varien_Data_Collection::SORT_ORDER_ASC );
                    }
                    else
                    {
                        $this->_collection->getSelect()->order('rand()');
                    }
                }
                else
                {
                    $this->_collection->getSelect()->order('rand()');
                }

                print $this->_collection->getSelect();
                die();
            }
            else
            {
                $fields = Mage::getSingleton('catalog/config')->getProductAttributes();

                $this->_collection = Mage::getResourceModel('catalog/product_collection')
                                        ->setStoreId(Mage::app()->getStore()->getId())
                                        // ->addAttributeToFilter('is_active', 1)
                                        ->addAttributeToSelect($fields)
                                        ->addMinimalPrice()
                                        ->addFinalPrice()
                                        ->addTaxPercents();

                if ($this->getCategories())
                {
                    $fields[] = 'at_category_id.category_id';

                    $this->_collection
                        ->addAttributeToSelect($fields)
                        ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left');

                    if ($flatFlag = Mage::getStoreConfigFlag(Mage_Catalog_Helper_Category_Flat::XML_PATH_IS_ENABLED_FLAT_CATALOG_CATEGORY))
                    {
                        $this->_collection->getSelect()->where('at_category_id.category_id in (?)', is_array($this->getCategories()) ? $this->getCategories() : explode(',', $this->getCategories()));
                    } else
                        $this->_collection->addAttributeToFilter( 'category_id', array('in' => is_array($this->getCategories()) ? $this->getCategories() : explode(',', $this->getCategories())) );
                }
                else if ($this->getProducts())
                {
                    $this->_collection->addIdFilter($this->getProducts());
                }

                $this->_collection->addAttributeToFilter('status', array('in'=>Mage::getSingleton('catalog/product_status')->getVisibleStatusIds()));
                $this->_collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
                //Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($this->_collection);
                //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_collection);

                $this->_collection->removeAttributeToSelect('category_id')->groupByAttribute('entity_id');

                if ($limit)
                    $this->_collection->getSelect()->limit($limit);

                if ($this->getSortBy())
                {
                    if ($sortField && $this->getSortType()!=2)
                    {
                        $this->_collection->setOrder($sortField, $this->getSortType() ? Varien_Data_Collection::SORT_ORDER_DESC : Varien_Data_Collection::SORT_ORDER_ASC );
                    }
                    else
                    {
                        $this->_collection->getSelect()->order('rand()');
                    }
                }
                else
                {
                    $this->_collection->getSelect()->order('rand()');
                }

                // print $this->_collection->getSelect(); die();
            }

        }

        return $this->_collection;
    }
}